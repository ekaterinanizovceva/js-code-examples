var scrollTop,
    selectTop = [],
    pagePosition = -1,
    sectId = $('.section-row');


function positionMenu() {
    sectId.each(function () {
        var _this = $(this),
            index = _this.data('index'),
            indexPosTop = _this.offset().top - 110,
            indexPosBot = indexPosTop + _this.height() - 110,
            indexName = _this.attr('id');
        selectTop[index] = ([indexPosTop, indexPosBot, indexName]);
    });
}

$(document).ready(function () {

    verify();

    // menu position
    positionMenu();
    $(window).resize(function () {
        positionMenu()
    });
    $(window).scroll(function () {
        scrollTop = $(window).scrollTop();
        sectId.each(function () {
            var index = $(this).data('index');
            if (selectTop[index][0] < scrollTop && selectTop[index][1] > scrollTop && pagePosition != index) {
                pagePosition = index;
                var redirect = '#' + selectTop[index][2];
                $('.js-menu li').removeClass('active');
                $('.js-menu li').eq(index).addClass('active');
            }
        })
    });

    $(document).on('click', '.js-anchor', function (e) {
        e.preventDefault();
        var clickHref = $(this).attr('href'),
            scrollPos = $(clickHref).offset().top - 80;
        $('html, body').animate({'scrollTop': scrollPos}, 1000)
    });


    // input validate
    $('.js-valid').keyup(function () {
        var validItem = $(this).parents('form').find('.js-valid'),
            validError = 0,
            validBtn = $(this).parents('form').find('.form-contact__btn input');
        validItem.each(function () {
            var _this = $(this),
                validStatus = _this.data('valid');
            if (validStatus == 'name') {
                if (_this.val().length < 2) {
                    validError = 1
                }
            }
            ;
            if (validStatus == 'email') {
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                    emailVal = _this.val();
                if (emailVal.length > 2) {
                    if (!emailReg.test(emailVal)) {
                        validError = 1
                    }
                } else {
                    validError = 1
                }

            }
        });

        if (validError == 0) {
            validBtn.removeAttr('disabled').addClass('active')
        } else {
            validBtn.attr('disabled', 'disabled').removeClass('active')
        }

    });


    $(document).on('submit', '.form-row', function (e) {
        e.preventDefault();
        var _form = $(this),
            formUrl = _form.data('url'),
            msg = _form.serialize();
        console.log(msg)
        $.ajax({
            type: "POST",
            url: formUrl,
            data: msg,
            success: function (data) {
                _form.trigger("reset");
            }
        });
        _form.addClass('forn-send');
        setTimeout('$(".form-row").removeClass("forn-send")', 2000);
        _form[0].reset()
    });


    $(document).on('click', '.js-scroll', function (e) {
        e.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);

    });


    // $('.winners-scroll').jScrollPane({
    //     autoReinitialise: true
    // });

    $('.winners-list').each(function () {
        var winnerList = $(this);
        $.ajax({
            type: 'GET',
            url: winnerList.data('winner-json'),
            dataType: 'json',
            success: function (data) {
                placePhotos(data, winnerList)
            },
            error: function (response) {
                console.log(response);
            }
        });
    });


    $('.fanc-box').fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        padding: 0,
        maxWidth: '100%',
        scrolling: 'visible',
        helpers: {
            media: {}
        }
    });

    $('.select-st').each(function () {
        var plach = $(this).attr('placeholder');
        $(this).select2({
            placeholder: plach,
            allowClear: true,
            "language": {
                "noResults": function () {
                    return "Результатов не найдено";
                }
            }
        });
    });

    $(document).on('click', '.inp-file__open', function (e) {
        e.preventDefault();
        $(this).parent().find('.inp-file__file').trigger('click');
        $(this).parent().find('i').remove();
    });
    $(document).on('change', '.inp-file__file', function (e) {
        e.preventDefault();

        var _this = $(this),
            fileAdress = _this.val();

        var file = e.target.files[0];

        _this.parent().find('.inp-file__txt').text(fileAdress);
        if (file.size < 5242880) {
            _this.parent('.inp-file').find('.inp-file__txt').removeAttr('style');
        }
        else {
            _this.parent('.inp-file').find('.inp-file__txt').css('border-color', 'red');
        }

        // var fileAdress = $(this).val();
        // $(this).parent().find('.inp-file__txt').text(fileAdress);
    });

    jQuery.validator.addMethod("noSpace", function (value, element) {
        return value.indexOf("  ") < 0 && value != "";
    }, "No space please and don't leave it empty");

    jQuery.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than {0}');

    $("#form-reg-check").validate({
        errorElement: "i",
        language: {
            infoEmpty: "Результатов не найдено"
        },
        rules: {
            reg_email: {
                required: true,
                noSpace: true,
                email: true
            },
            reg_name: {
                required: true,
                noSpace: true,
                minlength: 2
            },
            reg_surname: {
                required: true,
                noSpace: true,
                minlength: 2
            },
            reg_date_birth: {
                required: true,
                noSpace: true,
                minlength: 2
            },
            reg_phone: {
                required: true
            },
            check_compliance: {
                required: true
            },
            reg_date_purchase: {
                required: true,
                noSpace: true,
                minlength: 2
            },
            reg_check_number: {
                required: true,
                noSpace: true,
                minlength: 2
            },
            reg_number_products: {
                required: true
            },
            reg_purchase_amount: {
                required: true
            },
            reg_file_img: {
                required: true,
                filesize: 5242880
            },
            reg_city: {
                required: true,
                noSpace: true,
                minlength: 2
            },
            reg_drugstore: {
                required: true
            }
        },
        messages: {
            reg_email: {
                required: 'E-mail обязательно для заполнения',
                email: 'E-mail неправильно введен'
            },
            reg_name: {
                required: 'Обязательно для заполнения',
                minlength: 'Длина должна быть более 2-х символов'
            },
            reg_surname: {
                required: 'Обязательно для заполнения',
                minlength: 'Длина должна быть более 2-х символов'
            },
            reg_date_birth: {
                required: 'Обязательно для заполнения',
                minlength: 'Длина должна быть более 2-х символов'
            },
            reg_phone: {
                required: 'Обязательно для заполнения'
            },
            check_compliance: {
                required: 'Обязательно для заполнения'
            },
            reg_date_purchase: {
                required: 'Обязательно для заполнения',
                minlength: 'Длина должна быть более 2-х символов'
            },
            reg_check_number: {
                required: 'Обязательно для заполнения',
                minlength: 'Длина должна быть более 2-х символов'
            },
            reg_number_products: {
                required: 'Обязательно для заполнения'
            },
            reg_purchase_amount: {
                required: 'Обязательно для заполнения'
            },
            reg_file_img: {
                required: 'Обязательно для заполнения',
                filesize: 'Максимальный размер фото 5 Мб'
            },
            reg_city: {
                required: 'Обязательно для заполнения',
                minlength: 'Длина должна быть более 2-х символов'
            },
            reg_drugstore: {
                required: 'Обязательно для заполнения'
            }
        },
        submitHandler: function (form) {
            // form.submit();
            formSend();
            return false;
        }
    });


    $("#form-contacts").validate({
        language: {
            infoEmpty: "Результатов не найдено"
        },
        rules: {
            cont_email: {
                required: true,
                noSpace: true,
                email: true
            },
            cont_name: {
                required: true,
                noSpace: true,
                minlength: 2
            },
            cont_text: {
                required: true,
                noSpace: true,
                minlength: 2
            }
        },
        messages: {
            cont_email: {
                required: 'E-mail обязательно для заполнения',
                email: 'E-mail неправильно введен'
            },
            cont_name: {
                required: 'Обязательно для заполнения',
                minlength: 'Длина должна быть более 2-х символов'
            },
            cont_text: {
                required: 'Обязательно для заполнения',
                minlength: 'Длина должна быть более 2-х символов'
            }
        },
        submitHandler: function (form) {
            // form.submit();
            formSendCont();
            return false;
        }
    });


    $(document).on('click', '.info-down-check', function (e) {
        e.preventDefault();
        $(this).parent().next('.manual-check').slideToggle();
    });

    $(".mask").each(function () {
        var maskItem = $(this).data('mask');
        $(this).mask(maskItem);
    });


    $(document).on('submit', '#form-search', function (e) {
        e.preventDefault();
        $('.winners-list').each(function () {
            var winnerList = $(this),
                searchVal = $('.winners-search__input').val();
            if (searchVal.length == 0) {
                return false
            }
            ;
            $(this).find('.winners-name').remove();
            $.ajax({
                type: 'GET',
                url: winnerList.data('winner-json'),
                data: searchVal,
                dataType: 'json',
                success: function (data) {
                    placePhotos(data, winnerList)
                },
                error: function (response) {
                    console.log(response);
                }
            });
        });
        dataLayer.push({
            'event': 'uaevent',
            'eventCategory': urlPage,
            'eventAction': 'click: search'
        });
    });


    $('.popup-wr').hide();

    $(document).on('click', '.winners-menu a', function (e) {
        e.preventDefault();
        var index = $(this).parent().index();
        $('.winners-menu a').removeClass('active');
        $(this).addClass('active');
        $('.winners-row .winners-span6').removeClass('active');
        $('.winners-row .winners-span6').eq(index).addClass('active');
    });

    $(document).on('click', '.burger', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $('.menu').toggleClass('open');
        $('body').toggleClass('fix');
    });

    $(document).on('click', '.js-menu a', function (e) {
        if($(window).width() < 991) {
            $('.burger').removeClass('active');
            $('.menu').removeClass('open');
            $('body').removeClass('fix');
        }
    });

    //var devicejs = device.noConflict();

    mindbox = window.mindbox || function() { mindbox.queue.push(arguments); };
    mindbox.queue = mindbox.queue || [];
    mindbox('create', {
        projectSystemName: 'LorealAcd',
        brandSystemName: 'Vichy',
        pointOfContactSystemName: 'VichyPromo',
        projectDomain: 'loreal-acd-services.directcrm.ru'
    });
});

var fileTxt = $('.inp-file__txt').text();
function formSend() {
    if ($('#check-compliance:checked').length) {
        var _form = $('#form-reg-check'),
            formUrl = _form.data('url');

        var formData = new FormData($(_form)[0]);

        var fileInput = document.getElementById("check_upload");
        if (fileInput.files.length > 0) {
            formData.append("check_image", fileInput.files[0]);
        }

        $.ajax({
            type: "POST",
            url: formUrl,
            processData: false,
            contentType: false,
            data: formData,
            dataType: 'json',
            success: function (data) {
                $('.select-st').select2("val", "0")
                _form.trigger("reset");
                $('.inp-file__txt').text(fileTxt);

                $.fancybox.open([
                    {
                        href: '#reg-ok'
                    }
                ], {
                    padding: 0
                });
                setTimeout('$.fancybox.close()', 2000);
                $('.load').fadeOut();

                var inputData = {
                    firstName: $('#name-input').val(),
                    lastName: $('#surname-input').val(),
                    birthDate: $('#birthday-input').val(),
                    settlement: $('#city-input').val(),
                    mobilePhone: $('#mobile-input').val()
                };

                if ($('#check-dispatch:checked').length) {
                    mindbox('identify', {
                        operation: 'VichyPromoTrack',
                        identificator: {
                            provider: 'email',
                            identity: $('#email-input').val()
                        },
                        data: inputData
                    });
                } else {
                    mindbox('identify', {
                        operation: 'VichyPromoTrackUnsub',
                        identificator: {
                            provider: 'email',
                            identity: $('#email-input').val()
                        },
                        data: inputData
                    });
                }


                dataLayer.push({
                    'event': 'uaevent',
                    'eventCategory': urlPage,
                    'eventAction': 'Register a check',
                    'eventLabel': 'send form'
                });
            },
            error: function(xhr, str){
                $.each(xhr.responseJSON, function( index, value ) {
                    $('[name="' + index + '"]').addClass('error');
                });
                $('.load').fadeOut();
            }
        });

        $('.load').fadeIn();
    }
}

function formSendCont() {
    var _form = $('#form-contacts'),
        formUrl = _form.data('url'),
        msg = _form.serialize();

    $.ajax({
        type: "POST",
        url: formUrl,
        data: msg,
        dataType: 'json',
        success: function (data) {
            _form.trigger("reset");
            $.fancybox.open([
                {
                    href: '#contacts-ok'
                }
            ], {
                padding: 0
            });
            setTimeout('$.fancybox.close()', 2000);
            $('.load').fadeOut();
        }
    });

    $('.load').fadeIn();

    dataLayer.push({
        'event': 'uaevent',
        'eventCategory': urlPage,
        'eventAction': 'contact form'
    });
}


function placePhotos(data, itemThis) {
    var review,
        reviewsHTML = ' ',
        itemMax = data.length;

    if (itemMax == 0) {
        itemThis.closest('.winners-box').parent().remove();
        return;
    }

    for (var i = 0; i < itemMax; i++) {
        review = data[i];
        reviewsHTML += '<div class="winners-name"><span>' + review['data'] + '</span><span class="winners-name__email">' + review['email'] + '</span><strong>' + review['user'] + '</strong></div>';
    }

    itemThis.append(reviewsHTML);


};


// GTM
!function (a) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : "object" == typeof module && module.exports ? module.exports = a(require("jquery")) : a(jQuery)
}(function (a) {
    "use strict";
    var b, c, d, e, f = {
        minHeight: 0,
        elements: [],
        percentage: !0,
        userTiming: !1,
        pixelDepth: !1,
        nonInteraction: !1,
        gaGlobal: !1,
        gtmOverride: !1,
        trackerName: !1,
        dataLayer: "dataLayer"
    }, g = a(window), h = [], i = !1, j = 0;
    return a.scrollDepth = function (k) {
        function l(a, f, g, h) {
            var i = k.trackerName ? k.trackerName + ".send" : "send";
            e ? (e({
                event: "uaevent",
                eventCategory: urlPage,
                eventAction: a,
                eventLabel: f
            }), k.pixelDepth && arguments.length > 2 && g > j && (j = g, e({
                event: "uaevent",
                eventCategory: urlPage,
                eventAction: "Pixel Depth",
                eventLabel: p(g)
            })), k.userTiming && arguments.length > 3 && e({
                event: "ScrollTiming",
                eventCategory: urlPage,
                eventAction: a,
                eventLabel: f,
                eventTiming: h
            })) : (b && (window[d](i, "event", urlPage, a, f, 1, {nonInteraction: k.nonInteraction}), k.pixelDepth && arguments.length > 2 && g > j && (j = g, window[d](i, "event", urlPage, "Pixel Depth", p(g), 1, {nonInteraction: k.nonInteraction})), k.userTiming && arguments.length > 3 && window[d](i, "timing", urlPage, a, h, f)), c && (_gaq.push(["_trackEvent", urlPage, a, f, 1, k.nonInteraction]), k.pixelDepth && arguments.length > 2 && g > j && (j = g, _gaq.push(["_trackEvent", urlPage, "Pixel Depth", p(g), 1, k.nonInteraction])), k.userTiming && arguments.length > 3 && _gaq.push(["_trackTiming", urlPage, a, h, f, 100])))
        }

        function m(a) {
            return {
                "25%": parseInt(.25 * a, 10),
                "50%": parseInt(.5 * a, 10),
                "75%": parseInt(.75 * a, 10),
                "100%": a - 5
            }
        }

        function n(b, c, d) {
            a.each(b, function (b, e) {
                -1 === a.inArray(b, h) && c >= e && (l("scroll", b, c, d), h.push(b))
            })
        }

        function o(b, c, d) {
            a.each(b, function (b, e) {
                -1 === a.inArray(e, h) && a(e).length && c >= a(e).offset().top && (l("Elements", e, c, d), h.push(e))
            })
        }

        function p(a) {
            return (250 * Math.floor(a / 250)).toString()
        }

        function q() {
            s()
        }

        function r(a, b) {
            var c, d, e, f = null, g = 0, h = function () {
                g = new Date, f = null, e = a.apply(c, d)
            };
            return function () {
                var i = new Date;
                g || (g = i);
                var j = b - (i - g);
                return c = this, d = arguments, j <= 0 ? (clearTimeout(f), f = null, g = i, e = a.apply(c, d)) : f || (f = setTimeout(h, j)), e
            }
        }

        function s() {
            i = !0, g.on("scroll.scrollDepth", r(function () {
                var b = a(document).height(), c = window.innerHeight ? window.innerHeight : g.height(),
                    d = g.scrollTop() + c, e = m(b), f = +new Date - t;
                if (h.length >= k.elements.length + (k.percentage ? 4 : 0))return g.off("scroll.scrollDepth"), void(i = !1);
                k.elements && o(k.elements, d, f), k.percentage && n(e, d, f)
            }, 500))
        }

        var t = +new Date;
        k = a.extend({}, f, k), a(document).height() < k.minHeight || (k.gaGlobal ? (b = !0, d = k.gaGlobal) : "function" == typeof ga ? (b = !0, d = "ga") : "function" == typeof __gaTracker && (b = !0, d = "__gaTracker"), "undefined" != typeof _gaq && "function" == typeof _gaq.push && (c = !0), "function" == typeof k.eventHandler ? e = k.eventHandler : void 0 === window[k.dataLayer] || "function" != typeof window[k.dataLayer].push || k.gtmOverride || (e = function (a) {
                window[k.dataLayer].push(a)
            }), a.scrollDepth.reset = function () {
            h = [], j = 0, g.off("scroll.scrollDepth"), s()
        }, a.scrollDepth.addElements = function (b) {
            void 0 !== b && a.isArray(b) && (a.merge(k.elements, b), i || s())
        }, a.scrollDepth.removeElements = function (b) {
            void 0 !== b && a.isArray(b) && a.each(b, function (b, c) {
                var d = a.inArray(c, k.elements), e = a.inArray(c, h);
                -1 != d && k.elements.splice(d, 1), -1 != e && h.splice(e, 1)
            })
        }, q())
    }, a.scrollDepth
});

// dataLayer scroll
var scrollIndex = 0;
$(window).scroll(function () {
    if (scrollIndex == 0) {
        scrollIndex = 1;
        dataLayer.push({
            'event': 'uaevent',
            'eventCategory': urlPage,
            'eventAction': 'scroll',
            'eventLabel': 'fold'
        });
    }
});
jQuery(function () {
    jQuery.scrollDepth();
});

var urlPage = '/check/';
$(document).on('ready', function () {

    $(document).on('click', '.gtm-menu', function (e) {
        var href = $(this).text();
        dataLayer.push({
            'event': 'uaevent',
            'eventCategory': urlPage,
            'eventAction': 'click: menu',
            'eventLabel': href
        });

    });

    $(document).on('click', '.gtm-learn_more', function (e) {
        dataLayer.push({
            'event': 'uaevent',
            'eventCategory': urlPage,
            'eventAction': 'click: learn more'
        });
    });

    $(document).on('click', '.gtm-rules', function (e) {
        dataLayer.push({
            'event': 'uaevent',
            'eventCategory': urlPage,
            'eventAction': 'click: rules'
        });
    });

    $(document).on('click', '.gtm-reg-check', function (e) {
        dataLayer.push({
            'event': 'uaevent',
            'eventCategory': urlPage,
            'eventAction': 'Register a check',
            'eventLabel': 'open form'
        });
    });

    $(document).on('click', '.gtm-soc', function (e) {
        var soc = $(this).data('soc-name');
        dataLayer.push({
            'event': 'uaevent',
            'eventCategory': urlPage,
            'eventAction': 'сlick: social icon',
            'eventLabel': soc
        });
    });


});

function verify() {
    var verify = getUrlParameter('verify');

    if (verify != '') {
        $.fancybox.open([
            {
                href: '#reg-'+verify
            }
        ], {
            padding: 0
        });
    }
}

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}