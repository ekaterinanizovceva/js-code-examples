jQuery(function($) {

    var hash_popup = window.location.hash;
    if (hash_popup) {
        if (hash_popup == 'login') {
            $('.popup.popup--stop').addClass('popup--active');
        }
        else $('.popup' + hash_popup).addClass('popup--active');
    }

    heightMain();
    $(window).resize(function () {
        heightMain();
    });

    if (!(navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/))) {
        $('#upload-photo').attr('accept', '.jpg, .jpeg, .png');
    }

    /*
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	*/

    $('a.scroll[href*="#"]').click(function() {
        var h = $($.attr(this, 'href')).offset().top - 100;
        $('html, body').animate({ scrollTop: h }, (h/3));
        return false;
    });

    /*меню-dropdown*/
    $('.nav-modile').click(function(e) {
        var $this = $(this);

        if($this.hasClass('active')) {
            $this.removeClass('active');
        }
        else $this.addClass('active');

        if (!$this.find('.nav > li > a:not(.js-feedback)').is(e.target) && $this.find('.nav > li > a:not(.js-feedback)').has(e.target).length === 0) {

            return false;
        }
        else return true;
    });

    /*Popups*/
    $(document).mouseup(function (e){
        var div = $(".popup");
        if (!div.find('.popup-container').is(e.target) && div.find('.popup-container').has(e.target).length === 0) {

            if ($('popup--remove').is(e.target) && $('popup--active').is(e.target)) {
                location.reload();
                return false;
            }
            div.removeClass('popup--active');

            if (timerCheckShares) clearTimeout(timerCheckShares);
            if(div.find('form').length > 0) {
                div.find('form textarea').removeClass('error').val('');
                div.find('form input:not(.button)').removeClass('error').val('');
                div.find('form input[type="checkbox"]').removeAttr("checked");
                div.find('.popup-container').removeAttr('style');
                if ($('div').is('.g-recaptcha')) grecaptcha.reset();
            }
        }
    });
    $(document).keydown(function(e) {
        if (e.which == 27) {
            $('.popup').removeClass('popup--active');
            if (timerCheckShares) clearTimeout(timerCheckShares);
        }
    });
    $('.popup-close').click(function() {
        if (typeof $(this).parents('.popup').attr('data-redirect') !== "undefined") {
            window.location.href = $(this).parents('.popup').attr('data-redirect');
        }

        if (timerCheckShares) clearTimeout(timerCheckShares);

        if ($(this).parents('.popup').hasClass('popup--remove')) {
            location.reload();
            return false;
        }

        if ($(this).hasClass('share')) return true;
        else $(this).parents('.popup').removeClass('popup--active');

        if($(this).parents('.popup').find('form').length > 0) {
            $(this).parents('.popup').find('form textarea').removeClass('error').val('');
            $(this).parents('.popup').find('form input:not(.button)').removeClass('error').val('');
            $(this).parents('.popup').find('form input[type="checkbox"]').removeAttr("checked");
            $(this).find('.popup-container').removeAttr('style');
            if ($('div').is('.g-recaptcha')) grecaptcha.reset();
        }

        return false;
    });
    $('#vk_auth').click(function(e) {
        if (!$(this).find('iframe').is(e.target)
            && $(this).find('iframe').has(e.target).length === 0) {
            $(this).html('');
        }
        return false;
    });
    $('.js-login').click(function() {
        $('.popup').removeClass('popup--active');
        // $('.popup.popup--login').addClass('popup--active');
        $('.popup.popup--stop').addClass('popup--active');
        return false;
    });
    $('.js-feedback').click(function() {
        $('.popup').removeClass('popup--active');
        $('.popup.popup--feedback').addClass('popup--active');
        return false;
    });

    $('.js-registration').click(function() {
        $('.popup').removeClass('popup--active');
        $('.popup.popup--registration').addClass('popup--active');
        return false;
    });
    $('.js-recovery').click(function() {
        $('.popup').removeClass('popup--active');
        $('.popup.popup--recovery').addClass('popup--active');
        return false;
    });


    $('input[name="date"]').mask('99.99.9999');
    $('input[name="phone"]').mask('(999) 999-99-99');


    $('input[type="checkbox"]').click(function() {
        var $this = $(this);
        if($this.is(":checked")) {
            $this.val('yes');
            // $this.removeClass('error');
        } else {
            $this.val('no');
            // $this.addClass('error');
        }
    });

    $('.popup form').submit(function(e){
        e.preventDefault();

        var $this = $(this),
            ajax_url = $this.find('input[type="submit"]').data('href');

        $this.find('input').removeClass('error');
        $this.find('textarea').removeClass('error');
        $this.find('.form-row p.error-msg').remove();

        $.post(ajax_url, $(this).serialize(), function (data) {
            if (data['success'] == true) {

                if ($this.closest('.popup').attr('id') == 'login') {
                    window.location.href = route('front.cabinet.get');
                } else {
                    if ($this.closest('.popup').attr('id') == 'reset') {
                        $('.popup.popup--text').attr('data-redirect', route('front.index.get'));
                    } else {
                        $('.popup.popup--text').removeAttr('data-redirect');
                    }
                    $('.popup.popup--text').find('.popup-message').html('<h3>' + data['message'] + '</h3>');
                    $('.popup').removeClass('popup--active');
                    $('.popup.popup--text').addClass('popup--active');
                }
            }
        })
            .fail(function(xhr, status, err) {

                // console.log(code);

                var status = xhr.status;
                var message;

                if (status === 419 || status === 500) {
                    if (xhr.status === 500) {
                        message = 'Произошла ошибка. <br>Попробуйте позднее';
                    } else if (xhr.status === 419) {
                        message = 'Время сессии истекло. Страница будет перезагружена';
                    }

                    $('body').append('<div class="popup-common open backdrop-opaque go-page"><div class="popup"><p class="popup-tip">'+ message +'</p><a href="#" class="go-page skin-btn skin-btn-reg">Закрыть</a></div></div>');

                    $(document).trigger('ajaxBtnReceived');

                    return;
                }

                var responseJSON = $.parseJSON(xhr.responseText);

                if (responseJSON) {
                    var errors = responseJSON;
                    var field;
                    $.each( errors.errors, function( key, value ) {
                        field = $this.find('[name=' + key + ']');
                        field.addClass('error');

                        if (errors.errors[key].length > 1) {
                            for(var i = 0; i < errors.errors[key].length; i++) {

                                field.closest('.form-row').append('<p class="error-msg">' + errors.errors[key][i] + '</p>');
                            }
                        }
                        else {
                            field.closest('.form-row').append('<p class="error-msg">' + value + '</p>');
                        }
                        if (key === 'subscribe-agree') {
                            $('#popup-subscribe, #brands').find('[name=' + key + ']').addClass('error').closest('.form-row').append('<p class="error-msg">' + value + '</p>');
                            if (Object.keys(errors.errors).length === 1) {
                                needCustom = true;
                            }
                        }
                    });
                }
            });

    });


    /*Вывод участников (на главной)*/
    if ($('section').is('#section-participants')) {
        var url_gallery = $('#section-participants .section-participants__list').data('url');
        $.ajax({
            url: url_gallery,
            dataType: 'json',
            data: {
                page: participants_page,
                limit: participants_limit
            },
            success: function(data){
                displayParticipants(data);
                participants_page ++;

                $.ajax({
                    url: url_gallery,
                    dataType: 'json',
                    data: {
                        page: participants_page,
                        limit: participants_limit
                    },
                    success: function(data){
                        if (!data['items'].length > 0) {
                            $('#section-participants .more').css('display', 'none');
                        }
                    }
                });
            }
        });

        $('#section-participants').on('click', '.more', function() {
            var url_gallery = $('#section-participants .section-participants__list').data('url');
            $.ajax({
                url: url_gallery,
                dataType: 'json',
                data: {
                    page: participants_page,
                    limit: participants_limit
                },
                success: function(data){
                    displayParticipants(data);
                    participants_page ++;

                    $.ajax({
                        url: url_gallery,
                        dataType: 'json',
                        data: {
                            page: participants_page,
                            limit: participants_limit
                        },
                        success: function(data){
                            if (!data['items'].length > 0) {
                                $('#section-participants .more').css('display', 'none');
                            }
                        }
                    });
                }
            });
            return false;
        });

        $('#section-participants').on('click', '.section-participants__item', function(e) {
            var $this = $(this);
            if (!(!$this.find('.section-participants__nickname a span').is(e.target)
                && $this.find('.section-participants__nickname a span').has(e.target).length === 0)) {
                return true;
            }
            if (!$this.find('.popup').is(e.target)
                && $this.find('.popup').has(e.target).length === 0) {
                var img_src = $this.find('.popup.popup--participant .popup-container > img').data('src');
                $this.find('.popup.popup--participant .popup-container > img').attr('src', img_src);
                $this.find('.popup').addClass('popup--active');
                setTimeout(function() {
                    popupVerticalMiddle($this.find('.popup'));
                }, 500);
            }
        });
        $('#section-participants').on('click', '.popup-close', function() {
            $(this).parents('.popup').removeClass('popup--active');

            return false;
        });
    }


    /*Загрузка фото*/
    $('#upload-photo').change(function (e) {
        var $this = $(this);
        if ($this.val() == "") {
            return;
        }

        var file = e.target.files[0];

        if (file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg') {

            if (file.size < 4194304) {
                getOrientation(file, function(orientation) {

                    var fr = new FileReader();
                    fr.onload = (function(theFile) {
                        return function(e) {

                            $('.section-upload__item.upload--photo').find('.section-upload__item-field .section-upload__item-field__canvas').show();
                            $('.section-upload__item.upload--photo').find('.section-upload__item-field').css({
                                'background-image': 'none'
                            });

                            var img_crop = new Image(),
                                img_src = '';

                            if (orientation > 1) {
                                resetOrientation(e.target.result, orientation, function(resetBase64Image) {
                                    img_src = resetBase64Image;
                                    unloadPhoto (img_crop, img_src);
                                });
                            }
                            else {
                                img_src = e.target.result;
                                unloadPhoto (img_crop, img_src);
                            }

                        };
                    })(file);
                    fr.readAsDataURL(file);

                });
            }
            else {
                $('.popup.popup--text').find('.popup-message').html('<h3>Фото не должно превышать <span style="display:inline-block">4 Мб</span></h3>');
                $('.popup').removeClass('popup--active');
                $('.popup.popup--text').addClass('popup--active');
                // $this.val('');
                // $this.parents('form').find('.section-upload__item-field').removeAttr('style');
                // $this.parents('form').find('input#upload-photo--data64').val('');
            }
        }
        else {
            $('.popup.popup--text').find('.popup-message').html('<h3>Введен файл недопустимого типа</h3>');
            $('.popup').removeClass('popup--active');
            $('.popup.popup--text').addClass('popup--active');
            // $this.val('');
            // $this.parents('form').find('.section-upload__item-field').removeAttr('style');
            // $this.parents('form').find('input#upload-photo--data64').val('');
        }
    });


    $('.section-upload__item-field').on('click', 'a.button.crop-photo', function() {
        var data__cropper = cropper.getCroppedCanvas();
        var dataURL__cropper = data__cropper.toDataURL();

        $('label[for="upload-photo"]').show();
        $('a.button.crop-photo').hide();

        cropper.destroy();

        $('.section-upload__item.upload--photo').find('.section-upload__item-field').css({
            'background-image': 'url(' + dataURL__cropper + ')',
            'background-size': 'contain'
        });

        $('.section-upload__item.upload--photo').find('.section-upload__item-field .section-upload__item-field__canvas').hide();
        if($('.section-upload__item.upload--photo').find('.section-upload__item-field input#upload-photo--data64').length > 0)
            $('.section-upload__item.upload--photo').find('.section-upload__item-field input#upload-photo--data64').val(dataURL__cropper);
        else $('.section-upload__item.upload--photo').find('.section-upload__item-field').append('<input id="upload-photo--data64" type="hidden" value="' + dataURL__cropper + '"/>');

        return false;
    });



    /*Отправка фото*/
    $('#js-share').click(function() {
        var $this = $(this),
            $form = $this.parents('form'),
            $file = $form.find('input[name="upload-photo"]'),
            upload_text = $form.find('textarea[name="upload-text"]').val(),
            upload_date = new Date(),
            url = $('.data').data('add-url');

        if ($file.val() == '') {
            $('.popup').removeClass('popup--active');
            $('.popup.popup--text').find('.popup-message').html('<h3>Загрузите фотографию</h3>');
            $('.popup.popup--text').addClass('popup--active');
            return false;
        }
        else if (upload_text == '') {
            $('.popup').removeClass('popup--active');
            $('.popup.popup--text').find('.popup-message').html('<h3>Напишите свою историю</h3>');
            $('.popup.popup--text').addClass('popup--active');
            return false;
        }
        else {

            /*Запоминаем данные*/
            upload_data.date = upload_date.getDate() + '.' + upload_date.getMonth() + '.' + upload_date.getFullYear();
            upload_data.text = upload_text;
            upload_data.photo = $form.find('input#upload-photo--data64').val(); //e.target.result;
            /*end Запоминаем данные*/

            var formData = new FormData();
            formData.append('photo', upload_data.photo);
            formData.append('text', upload_text);

            $.ajax({
                type: $('.data').data('add-type'),
                url: $('.data').data('add-url'),
                data: formData,
                processData: false,
                contentType: false,
                success: function(data) {
                    upload_data.id = data['id'];
                    upload_data.urls = data['urls'];
                    $('.popup-share-image img').attr('src', data.img);
                    $('.popup.popup--share').addClass('popup--active');
                    setTimeout(function() {
                        popupVerticalMiddle($this.parents('.popup'));
                    }, 1000);
                    pageURL = data.url;
                    pageIMG = data.img;
                },
                error: function(jqXHR, exception) {
                    alert('ошибка: ' + jqXHR.status + ', exception: ' + exception );
                },
                beforeSend: function() {
                    $('#loader').show();
                },
                complete: function() {
                    $('#loader').hide();
                }
            })
                .fail(function(xhr, status, err) {
                    var status = xhr.status;
                    var message;
                    if (status === 419 || status === 500) {
                        if (xhr.status === 500) {
                            message = 'Произошла ошибка. <br>Попробуйте позднее';
                        } else if (xhr.status === 419) {
                            message = 'Время сессии истекло. Страница будет перезагружена';
                        }
                        $('.popup').removeClass('popup--active');
                        $('.popup.popup--text').find('.popup-message').html('<h3>' + message + '</h3>');
                        $('.popup.popup--text').addClass('popup--active');
                        $(document).trigger('ajaxBtnReceived');
                        return;
                    }

                    var responseJSON = $.parseJSON(xhr.responseText);
                    if (responseJSON) {
                        var errors = responseJSON;
                        $.each( errors.errors, function( key, value ) {
                            $('.popup').removeClass('popup--active');
                            $('.popup.popup--text').find('.popup-message').html('<h3>' + value + '</h3>');
                            $('.popup.popup--text').addClass('popup--active');
                        });
                    }
                });
        }

        return false;
    });

    /*Удаление фото*/
    $('.section-works').on('click', '.section-works__remove', function() {
        var $this = $(this),
            $item = $this.parents('.section-works__item'),
            i = $('.section-works .section-works__list .section-works__item').index($item);

        remove_item_id = $item.data('id');

        $('.popup.popup--remove-yes-no').addClass('popup--active');
        return false;
    });
    $('.popup.popup--remove-yes-no').on('click', '.button.yes', function() {
        $('.popup').removeClass('popup--active');

        $.ajax({
            type: $('.data').data('remove-type'),
            url: $('.data').data('remove-url'),
            data: {
                'id': remove_item_id
            },
            success: function(data) {
                if(data['success'] == true) {
                    $('.popup').removeClass('popup--active');
                    $('.popup.popup--remove').addClass('popup--active');
                }
            },
            error: function() {}
        });

        return false;
    });
    $('.popup.popup--remove').on('click', '.button', function() {
        location.reload();
        return false;
    });


    /*перетаскивание фвйла в input[type="file"]*/
    if ($('.section-upload__item.upload--photo .section-upload__item-field').length > 0) {
        var dropZone = $('.section-upload__item.upload--photo .section-upload__item-field');

        // Проверка поддержки браузером
        if (typeof(window.FileReader) == 'undefined') {
            dropZone.text('Не поддерживается браузером!');
            dropZone.addClass('error');
        }

        // Добавляем класс hover при наведении
        dropZone[0].ondragover = function() {
            dropZone.addClass('hover');
            return false;
        };

        // Убираем класс hover
        dropZone[0].ondragleave = function() {
            dropZone.removeClass('hover');
            return false;
        };

        // Обрабатываем событие Drop
        dropZone[0].ondrop = function(event) {
            event.preventDefault();
            dropZone.removeClass('hover');
            dropZone.addClass('drop');

            if (event.dataTransfer.files[0].type == 'image/png' || event.dataTransfer.files[0].type == 'image/jpg' || event.dataTransfer.files[0].type == 'image/jpeg') {
                $('#upload-photo')[0].files = event.dataTransfer.files;

                var f = event.dataTransfer.files[0];

                if (file.size < 4194304) {
                    getOrientation(file, function(orientation) {

                        var fr = new FileReader();
                        fr.onload = (function(theFile) {
                            return function(e) {

                                $('.section-upload__item.upload--photo').find('.section-upload__item-field .section-upload__item-field__canvas').show();
                                $('.section-upload__item.upload--photo').find('.section-upload__item-field').css({
                                    'background-image': 'none'
                                });

                                var img_crop = new Image(),
                                    img_src = '';

                                if (orientation > 1) {
                                    resetOrientation(e.target.result, orientation, function(resetBase64Image) {
                                        img_src = resetBase64Image;
                                        unloadPhoto (img_crop, img_src);
                                    });
                                }
                                else {
                                    img_src = e.target.result;
                                    unloadPhoto (img_crop, img_src);
                                }

                            };
                        })(file);
                        fr.readAsDataURL(file);

                    });
                }
                else {
                    $('#upload-photo').val('');
                    $('.popup.popup--text').find('.popup-message').html('<h3>Фото не должно превышать <span style="display:inline-block">4 Мб</span></h3>');
                    $('.popup').removeClass('popup--active');
                    $('.popup.popup--text').addClass('popup--active');
                    // $this.val('');
                    // $this.parents('form').find('.section-upload__item-field').removeAttr('style');
                    // $this.parents('form').find('input#upload-photo--data64').val('');
                }
            }
            else {
                $('.popup.popup--text').find('.popup-message').html('<h3>Введен файл недопустимого типа</h3>');
                $('.popup').removeClass('popup--active');
                $('.popup.popup--text').addClass('popup--active');
            }
        };
    }



    var originalAddClassMethod = $.fn.addClass;
    var originalRemoveClassMethod = $.fn.removeClass;
    $.fn.addClass = function(){
        var result = originalAddClassMethod.apply( this, arguments );
        $(this).trigger('classChanged');
        return result;
    }
    $.fn.removeClass = function(){
        var result = originalRemoveClassMethod.apply( this, arguments );
        $(this).trigger('classChanged');
        return result;
    }
    $('.popup').bind('classChanged', function(){
        // console.log('class changed');
        var $this = $(this);
        if($this.hasClass('popup--active')) {
            popupVerticalMiddle($this);

            if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
                $('body').addClass('body-modal-open');
            }
        }
        else {
            $this.removeAttr('style');
            $this.parents('body').removeClass('body-modal-open');
        }
    });




    /* new */
    var $window = $(window);

    /* Options for auth modal window
    ***********************************************/
    var popWinOptions = function () {
        var options = ['modal=yes'],
            winWidth = $window.width(),
            winHeight = $window.height();
        if (winWidth > 600) options.push('width=600,left=' + ((winWidth - 600) / 2));
        if (winHeight > 600) options.push('height=600,top=' + ((winHeight - 600) / 2));
        return options.join();
    };


    // init social VK
    if ('VK' in window) {
        VK.init({ apiId: 6450668 });
    }

    // init social FB
    window.fbAsyncInit = function () {
        FB.init({
            appId: '245366416010672',
            cookie: true,
            xfbml: true,
            version: 'v2.8'
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    // init social OK
    var okAppID = 1265818112,
        okAppPublicKey = 'CBALNDHMEBABABABA',
        okRedirectURI = encodeURIComponent('https://msmama.ru/page/cabinet'),
        okLayoutDevice = $window.width() < 641 ? 'm' : 'w',
        okUrl = 'https://connect.ok.ru/oauth/authorize?client_id=' + okAppID + '&response_type=token&layout=' + okLayoutDevice + '&redirect_uri=' + okRedirectURI,
        okAuthWindow;

    if (!window.ODKL) {
        window.ODKL = {};
    }

    $(document).on('click', '[data-auth]', function (e) {
        if (timerCheckShares) clearTimeout(timerCheckShares);

        setTimeout("$('[data-auth]').attr('href', '#')", 3000);
        var $this = $(this);
        if ($this.attr('href') == "#") {
            e.preventDefault();
            player.network = $this.data('auth');

            // vk
            if (player.network == 'vk') {
                $('#vk_auth_wrapper').show();
                VK.Widgets.Auth('vk_auth', {
                    onAuth: function (data) {
                        $('#vk_auth_wrapper').hide();
                        $('#vk_auth').hide().html('').removeAttr('style');
                        player.id = data.uid;
                        player.name = data.first_name + ' ' + data.last_name;
                        player.photo = data.photo;

                        // user send
                        //shareSend(player.network);
                        $('.popup.popup--text').find('.popup-message').html('<h3>Авторизация прошла успешно!</h3>');
                        $('.popup.popup--text').find('.button').attr('onClick', 'shareSend();return false;').addClass('share').text('Поделиться');
                        $('.popup').removeClass('popup--active');
                        $('.popup.popup--text').addClass('popup--active');
                    }
                });

            }

            // fb
            if (player.network == 'fb') {
                FB.login(function (response) {
                    if (response.status === 'connected') {
                        player.id = response.authResponse.userID;
                        FB.api(
                            '/' + player.id,
                            { fields: 'first_name,last_name,friends,picture.width(2048)' },
                            function (response) {
                                if (response && !response.error) {
                                    player.name = response.first_name + ' ' + response.last_name;
                                    player.photo = response.picture.data.url;
                                    // user send
                                    //shareSend(player.network);
                                    $('.popup.popup--text').find('.popup-message').html('<h3>Авторизация прошла успешно!</h3>');
                                    $('.popup.popup--text').find('.button').attr('onClick', 'shareSend();return false;').addClass('share').text('Поделиться');
                                    $('.popup').removeClass('popup--active');
                                    $('.popup.popup--text').addClass('popup--active');
                                }
                            }
                        );

                    } else {
                        alert('Ошибка входа в аккаунт')
                    }
                });
            }

            // ok
            if (player.network == 'ok') {
                if (okAuthWindow) {
                    okAuthWindow.focus();
                } else {
                    okAuthWindow = window.open(okUrl, 'Авторизация', popWinOptions());
                    // Запускаем таймер проверки токена в урле
                    var timer = setInterval(function () {
                        var hash, token;
                        try { hash = okAuthWindow.location.hash } catch (e) { } // try для исключения ошибки безопасности кроссдоменного прослушивания урла окна
                        if (hash && hash.indexOf('access_token=') > -1) { // токен появился в урле (когда домен окна стал нашим (redirectURI))
                            token = hash.split('access_token=')[1].split('&')[0];
                            var session = hash.split('session_secret_key=')[1].split('&')[0];
                            clearInterval(timer);

                            // Рассчитываем подпись для запроса данных о юзере
                            var sign = MD5('application_key=' + okAppPublicKey + 'fields=first_name,last_name,photo_id,pic1024x768method=users.getCurrentUser' + session);

                            $.ajax({
                                type: 'post',
                                url: 'https://api.ok.ru/fb.do?application_key=' + okAppPublicKey + '&fields=first_name,last_name,photo_id,pic1024x768&method=users.getCurrentUser&access_token=' + token + '&sig=' + sign,
                                dataType: 'xml',
                                success: function (data) {
                                    var $xml = $(data); // От сервера приходит XML
                                    player.id = $xml.find('uid').text();
                                    player.name = $xml.find('first_name').text() + ' ' + $xml.find('last_name').text();
                                    player.photo = $xml.find('photo_id').length ? $xml.find('pic1024x768').text() : false;

                                    // screenSlider.go(game.helpId);
                                    okAuthWindow.close();
                                    //okAuthWindow = null;
                                    // user send
                                    //shareSend(player.network);
                                    $('.popup.popup--text').find('.popup-message').html('<h3>Авторизация прошла успешно!</h3>');
                                    $('.popup.popup--text').find('.button').attr('onClick', 'shareSend();return false;').addClass('share').text('Поделиться');
                                    $('.popup').removeClass('popup--active');
                                    $('.popup.popup--text').addClass('popup--active');
                                },
                                error: function () {
                                    alert('Ошибка входа в аккаунт');
                                    okAuthWindow.close();
                                    //okAuthWindow = null;
                                }
                            });

                        }
                        if (hash && hash.indexOf('#error') > -1) { // Юзер не захотел авторизоваться
                            clearInterval(timer);
                            alert('Ошибка входа в аккаунт');
                            okAuthWindow.close();
                            //okAuthWindow = null;
                        }
                        if (okAuthWindow.closed) {
                            clearInterval(timer);
                            //okAuthWindow = null;
                        }
                    }, 200);
                }
            }
        };
    });

});

var player = {
        network: '',
        id: '',
        name: '',
        photo: 0,
        result: '',
        background: '',
        _token: $('meta[name="csrf-token"]').attr('content')
    },
    upload_data = {
        id: 0,
        date: '',
        photo: '',
        text: '',
        urls: ''
    },
    remove_item_id = 0,
    participants_page = 0,
    participants_limit = 8;

if ($(window).width() < 500 ) {
    participants_limit = 4;
}
else participants_limit = 8;

var timerCheckShares;


var cropper;


/* MD5 encoding
***********************************************/
function MD5(val) { function md5cycle(f, c) { var b = f[0], a = f[1], d = f[2], e = f[3]; b = ff(b, a, d, e, c[0], 7, -680876936), e = ff(e, b, a, d, c[1], 12, -389564586), d = ff(d, e, b, a, c[2], 17, 606105819), a = ff(a, d, e, b, c[3], 22, -1044525330), b = ff(b, a, d, e, c[4], 7, -176418897), e = ff(e, b, a, d, c[5], 12, 1200080426), d = ff(d, e, b, a, c[6], 17, -1473231341), a = ff(a, d, e, b, c[7], 22, -45705983), b = ff(b, a, d, e, c[8], 7, 1770035416), e = ff(e, b, a, d, c[9], 12, -1958414417), d = ff(d, e, b, a, c[10], 17, -42063), a = ff(a, d, e, b, c[11], 22, -1990404162), b = ff(b, a, d, e, c[12], 7, 1804603682), e = ff(e, b, a, d, c[13], 12, -40341101), d = ff(d, e, b, a, c[14], 17, -1502002290), a = ff(a, d, e, b, c[15], 22, 1236535329), b = gg(b, a, d, e, c[1], 5, -165796510), e = gg(e, b, a, d, c[6], 9, -1069501632), d = gg(d, e, b, a, c[11], 14, 643717713), a = gg(a, d, e, b, c[0], 20, -373897302), b = gg(b, a, d, e, c[5], 5, -701558691), e = gg(e, b, a, d, c[10], 9, 38016083), d = gg(d, e, b, a, c[15], 14, -660478335), a = gg(a, d, e, b, c[4], 20, -405537848), b = gg(b, a, d, e, c[9], 5, 568446438), e = gg(e, b, a, d, c[14], 9, -1019803690), d = gg(d, e, b, a, c[3], 14, -187363961), a = gg(a, d, e, b, c[8], 20, 1163531501), b = gg(b, a, d, e, c[13], 5, -1444681467), e = gg(e, b, a, d, c[2], 9, -51403784), d = gg(d, e, b, a, c[7], 14, 1735328473), a = gg(a, d, e, b, c[12], 20, -1926607734), b = hh(b, a, d, e, c[5], 4, -378558), e = hh(e, b, a, d, c[8], 11, -2022574463), d = hh(d, e, b, a, c[11], 16, 1839030562), a = hh(a, d, e, b, c[14], 23, -35309556), b = hh(b, a, d, e, c[1], 4, -1530992060), e = hh(e, b, a, d, c[4], 11, 1272893353), d = hh(d, e, b, a, c[7], 16, -155497632), a = hh(a, d, e, b, c[10], 23, -1094730640), b = hh(b, a, d, e, c[13], 4, 681279174), e = hh(e, b, a, d, c[0], 11, -358537222), d = hh(d, e, b, a, c[3], 16, -722521979), a = hh(a, d, e, b, c[6], 23, 76029189), b = hh(b, a, d, e, c[9], 4, -640364487), e = hh(e, b, a, d, c[12], 11, -421815835), d = hh(d, e, b, a, c[15], 16, 530742520), a = hh(a, d, e, b, c[2], 23, -995338651), b = ii(b, a, d, e, c[0], 6, -198630844), e = ii(e, b, a, d, c[7], 10, 1126891415), d = ii(d, e, b, a, c[14], 15, -1416354905), a = ii(a, d, e, b, c[5], 21, -57434055), b = ii(b, a, d, e, c[12], 6, 1700485571), e = ii(e, b, a, d, c[3], 10, -1894986606), d = ii(d, e, b, a, c[10], 15, -1051523), a = ii(a, d, e, b, c[1], 21, -2054922799), b = ii(b, a, d, e, c[8], 6, 1873313359), e = ii(e, b, a, d, c[15], 10, -30611744), d = ii(d, e, b, a, c[6], 15, -1560198380), a = ii(a, d, e, b, c[13], 21, 1309151649), b = ii(b, a, d, e, c[4], 6, -145523070), e = ii(e, b, a, d, c[11], 10, -1120210379), d = ii(d, e, b, a, c[2], 15, 718787259), a = ii(a, d, e, b, c[9], 21, -343485551); f[0] = add32(b, f[0]); f[1] = add32(a, f[1]); f[2] = add32(d, f[2]); f[3] = add32(e, f[3]) } function cmn(f, c, b, a, d, e) { c = add32(add32(c, f), add32(a, e)); return add32(c << d | c >>> 32 - d, b) } function ff(f, c, b, a, d, e, g) { return cmn(c & b | ~c & a, f, c, d, e, g) } function gg(f, c, b, a, d, e, g) { return cmn(c & a | b & ~a, f, c, d, e, g) } function hh(f, c, b, a, d, e, g) { return cmn(c ^ b ^ a, f, c, d, e, g) } function ii(f, c, b, a, d, e, g) { return cmn(b ^ (c | ~a), f, c, d, e, g) } function md51(f) { var c = f.length, b = [1732584193, -271733879, -1732584194, 271733878], a; for (a = 64; a <= f.length; a += 64)md5cycle(b, md5blk(f.substring(a - 64, a))); f = f.substring(a - 64); var d = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; for (a = 0; a < f.length; a++)d[a >> 2] |= f.charCodeAt(a) << (a % 4 << 3); d[a >> 2] |= 128 << (a % 4 << 3); if (55 < a) for (md5cycle(b, d), a = 0; 16 > a; a++)d[a] = 0; d[14] = 8 * c; md5cycle(b, d); return b } function md5blk(f) { var c = [], b; for (b = 0; 64 > b; b += 4)c[b >> 2] = f.charCodeAt(b) + (f.charCodeAt(b + 1) << 8) + (f.charCodeAt(b + 2) << 16) + (f.charCodeAt(b + 3) << 24); return c } var hex_chr = "0123456789abcdef".split(""); function rhex(f) { for (var c = "", b = 0; 4 > b; b++)c += hex_chr[f >> 8 * b + 4 & 15] + hex_chr[f >> 8 * b & 15]; return c } function hex(f) { for (var c = 0; c < f.length; c++)f[c] = rhex(f[c]); return f.join("") } function md5(f) { return hex(md51(f)) } function add32(f, c) { return f + c & 4294967295 } "5d41402abc4b2a76b9719d911017c592" != md5("hello") && (add32 = function (f, c) { var b = (f & 65535) + (c & 65535); return (f >> 16) + (c >> 16) + (b >> 16) << 16 | b & 65535 }); return md5(val + '') }







/**
 * Проверка шеринга в соцсетях, если хотябы в одной пошерино, вызывается success( soc, count )
 * @param {string=} soc Необязательный параметр соцсети: 'vk', 'fb' или 'ok'
 * @param {Function=} success
 */

var pageURL, pageIMG;
var checkShares = (function () {
    var shared = false;
    return function (soc, success) {
        // VK
        function checkVK(success) {
            if (shared) return;
            if (!window.VK) window.VK = {};
            if (!window.VK.Share) window.VK.Share = {
                count: function (temp, count) {
                    if (shared) return;
                    if (+count > 0) {
                        shared = true;
                        codeShow()
                        // if (success) codeShow();
                    } else {
                        timerCheckShares = setTimeout(function () { checkVK(success) }, 3000);
                    }
                }
            };
            $.getScript('https://vk.com/share.php?act=count&index=0&url=' + encodeURI(pageURL));
        }
        // FB
        function checkFB() {
            if (shared) return;
            $.ajax({
                url: 'https://graph.facebook.com/' + encodeURIComponent(pageURL),
                dataType: 'json',
                success: function (data) {
                    if (shared) return;
                    if (data && data.share && +data.share.share_count > 0) {
                        codeShow()
                        // if (success) codeShow();
                    } else {
                        timerCheckShares = setTimeout(function () { checkFB(success) }, 3000);
                    }
                },
                error: function () {
                    timerCheckShares = setTimeout(function () { checkFB(success) }, 3000);
                }
            });
        }
        // OD
        function checkOD(success) {
            if (shared) return;
            if (!window.ODKL) window.ODKL = {};
            if (!window.ODKL.updateCount) window.ODKL.updateCount = function (temp, count) {
                if (shared) return;
                if (+count > 0) {
                    shared = true;
                    codeShow();
                    // if (success) codeShow();
                } else {
                    timerCheckShares = setTimeout(function () { checkOD(success) }, 3000);
                }
            };
            $.getScript('https://connect.ok.ru/dk?st.cmd=extLike&uid=odklocs0&ref=' + encodeURIComponent(pageURL));
        }

        if (soc == 'vk') checkVK(success);
        else if (soc == 'fb') checkFB(success);
        else if (soc == 'ok') checkOD(success);
        else {
            checkVK(success);
            checkFB(success);
            checkOD(success);
        }
    }
})();

function codeShow() {

    $('.popup').removeClass('popup--active');

    $.ajax({
        type: $('.data').data('share-type'),
        url: $('.data').data('share-url'),
        dataType: 'json',
        data: {
            'player': player,
            'id': upload_data['id'],
        },
        success: function (data) {
            location.reload();
        }
    });
}


/*----------------------------
	отправляем инфу о шеринге
----------------------------*/
function shareSend() {

    // if($('meta[property="og:image"]').length > 0)
    // 	$('meta[property="og:image"]').attr('content', pageIMG);
    // else $('head').append('<meta property="og:image" content="' + pageIMG + '">');

    var urlVK = 'https://vk.com/share.php?image='+pageIMG+'&url=' + pageURL;
    $('[data-auth=vk]').attr('href', urlVK);
    var urlOK = 'https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl='+pageURL;
    $('[data-auth=ok]').attr('href', urlOK);
    var urlFB = 'https://www.facebook.com/sharer.php?u='+pageURL;
    $('[data-auth=fb]').attr('href', urlFB);
    var link = $('[data-auth=' + player.network +']')[0];
    var linkEvent = document.createEvent('MouseEvents');
    linkEvent.initEvent('click', true, true);
    link.dispatchEvent(linkEvent);

    checkShares(player.network, false);
}



/* ---------------------- */


/* загрузка фото для кропа */
function unloadPhoto (img_crop, img_src) {
    $(img_crop).attr('src', img_src);
    $(img_crop).attr('id', 'crop-image');
    $('.section-upload__item.upload--photo').find('.section-upload__item-field .section-upload__item-field__canvas').html(img_crop);

    if($('.section-upload__item.upload--photo').find('.section-upload__item-field input#upload-photo--data64').length > 0)
        $('.section-upload__item.upload--photo').find('.section-upload__item-field input#upload-photo--data64').val(img_src);
    else $('.section-upload__item.upload--photo').find('.section-upload__item-field').append('<input id="upload-photo--data64" type="hidden" value="' + img_src + '"/>');

    $('label[for="upload-photo"]').hide();
    $('a.button.crop-photo').show();

    $(img_crop).cropper({
        viewMode: 3,
        dragMode: 'move',
        aspectRatio: 1 / 1
    });
    cropper = $(img_crop).data('cropper');
}


/* Добавление работы */
function addItemWorks (data) {
    var $works = $('.section-works .section-works__list');

    var item_html =
        '<div class="section-works__item" data-id="' + data.id + '">' +
        '<div class="section-works__photo" style="background-image: url(\'' + data.photo + '\');">' +
        '<p class="section-works__date">' + data.date + '</p>' +
        '<div class="section-works__remove">удалить</div>' +
        '</div>' +
        '<div class="section-works__text">' +
        '<p>' + data.text + '</p>' +
        '</div>' +
        '<div class="section-works__status orange">' +
        '<p>на модерации</p>' +
        '</div>' +
        '</div>';

    $works.find('a.section-works__item.item--upload-more:first').replaceWith(item_html);

    var length = $works.find('a.section-works__item.item--upload-more').length;

    if($works.find('a.section-works__item.item--upload-more').length < 1) {
        $('#section-upload').css('display', 'none');
    }
    return true;
}

/* Удаление работы */
function removeItemWorks (id) {
    var $works = $('.section-works .section-works__list');

    var item_html =
        '<a href="#section-upload" class="section-works__item item--upload-more">' +
        '<span>Можно загрузить еще</span>' +
        '</a>';

    $works.find('div.section-works__item[data-id="' + id + '"]').remove();
    $works.append(item_html);

    var length = $works.find('a.section-works__item.item--upload-more').length;

    if($works.find('a.section-works__item.item--upload-more').length > 0) {
        $('#section-upload').css('display', 'block');
    }

    return true;
}

function popupVerticalMiddle($this) {
    var h_w = $(window).height();
    var h = $this.find('.popup-container').innerHeight();
    if (h < h_w) {
        $this.find('.popup-container').css({
            'margin-top': '-' + (h / 2) + 'px',
            'margin-bottom': 0,
            'top': '50%',
        });
    }
}

function displayParticipants(data) {
    var $list = $('#section-participants .section-participants__list'),
        $item_default = $list.find('.section-participants__item.item-template');

    for(var i = 0; i < data['items'].length; i++) {
        var $item = $item_default.clone().removeClass('hidden').removeClass('item-template').addClass('post-' + data['items'][i]['post']['id']);

        $item.find('.section-participants__intro').text(data['items'][i]['post']['caption'].substr(0, 37));
        $item.find('.section-participants__photo').attr('style', 'background-image: url(' + data['items'][i]['post']['thumb'] + ');');
        $item.find('.popup.popup--participant .section-participants__text').text(data['items'][i]['post']['caption']);
        $item.find('.popup.popup--participant .popup-container > img').data('src', data['items'][i]['post']['src']);// .attr('src', data['items'][i]['post']['src']);

        if (data['items'][i]['user']['thumb']) {
            $item.find('.section-participants__avatar img').attr('src', data['items'][i]['user']['thumb']);
            $item.find('.popup.popup--participant .section-participants__avatar img').attr('src', data['items'][i]['user']['thumb']);
        }
        else {
            $item.find('.section-participants__avatar img').attr('src', './img/avatar.png');
            $item.find('.popup.popup--participant .section-participants__avatar img').attr('src', './img/avatar.png');
        }
        if (data['items'][i]['user']['link'] == '#') {
            if (data['items'][i]['user']['name'].substr(0, 1) == '@') {
                $item.find('.section-participants__nickname').html('@<span>' + data['items'][i]['user']['name'].substr(1) + '</span>');
                $item.find('.popup.popup--participant .section-participants__nickname').html('@<span>' + data['items'][i]['user']['name'].substr(1) + '</span>');
            }
            else {
                $item.find('.section-participants__nickname').html('<span>' + data['items'][i]['user']['name'] + '</span>');
                $item.find('.popup.popup--participant .section-participants__nickname').html('<span>' + data['items'][i]['user']['name'] + '</span>');
            }
        }
        else {
            if (data['items'][i]['user']['name'].substr(0, 1) == '@') {
                $item.find('.section-participants__nickname').html('<a href="' + data['items'][i]['user']['link'] + '" target="_blank">@<span>' + data['items'][i]['user']['name'].substr(1) + '</span></a>');
                $item.find('.popup.popup--participant .section-participants__nickname').html('<a href="' + data['items'][i]['user']['link'] + '" target="_blank">@<span>' + data['items'][i]['user']['name'].substr(1) + '</span></a>');
            }
            else {
                $item.find('.section-participants__nickname').html('<a href="' + data['items'][i]['user']['link'] + '" target="_blank"><span>' + data['items'][i]['user']['name'] + '</span></a>');
                $item.find('.popup.popup--participant .section-participants__nickname').html('<a href="' + data['items'][i]['user']['link'] + '" target="_blank"><span>' + data['items'][i]['user']['name'] + '</span></a>');
            }
        }

        $list.append($item);
    }
}


function resetOrientation(srcBase64, srcOrientation, callback) {

    var img = new Image();

    img.onload = function() {
        var width = img.width,
            height = img.height,
            canvas = document.createElement('canvas'),
            ctx = canvas.getContext("2d");

        // set proper canvas dimensions before transform & export
        if (4 < srcOrientation && srcOrientation < 9) {
            canvas.width = height;
            canvas.height = width;
        } else {
            canvas.width = width;
            canvas.height = height;
        }

        // transform context before drawing image
        switch (srcOrientation) {
            case 2: ctx.transform(-1, 0, 0, 1, width, 0); break;
            case 3: ctx.transform(-1, 0, 0, -1, width, height ); break;
            case 4: ctx.transform(1, 0, 0, -1, 0, height ); break;
            case 5: ctx.transform(0, 1, 1, 0, 0, 0); break;
            case 6: ctx.transform(0, 1, -1, 0, height , 0); break;
            case 7: ctx.transform(0, -1, -1, 0, height , width); break;
            case 8: ctx.transform(0, -1, 1, 0, 0, width); break;
            default: break;
        }

        // draw image
        ctx.drawImage(img, 0, 0);

        // export base64
        callback(canvas.toDataURL());
    };

    img.src = srcBase64;
}
function getOrientation(file, callback) {
    var reader = new FileReader();

    reader.onload = function(event) {
        var view = new DataView(event.target.result);

        if (view.getUint16(0, false) != 0xFFD8) return callback(-2);
        var length = view.byteLength,
            offset = 2;

        while (offset < length) {
            var marker = view.getUint16(offset, false);
            offset += 2;

            if (marker == 0xFFE1) {
                if (view.getUint32(offset += 2, false) != 0x45786966) {
                    return callback(-1);
                }
                var little = view.getUint16(offset += 6, false) == 0x4949;
                offset += view.getUint32(offset + 4, little);
                var tags = view.getUint16(offset, little);
                offset += 2;

                for (var i = 0; i < tags; i++)
                    if (view.getUint16(offset + (i * 12), little) == 0x0112)
                        return callback(view.getUint16(offset + (i * 12) + 8, little));
            }
            else if ((marker & 0xFF00) != 0xFF00) break;
            else offset += view.getUint16(offset, false);
        }
        return callback(-1);
    };
    reader.readAsArrayBuffer(file.slice(0, 64 * 1024));
};

var height_main__curr = $('.section-main').innerHeight(),
    height_iframe_curr = $('.section-main__iframe').innerHeight();

function heightMain() {
    var $main = $('.section-main'),
        $iframe = $('.section-main__iframe');

    if ($(window).width() > 1220 ) {
        var height_window = $(window).innerHeight(),
            height_main = height_window - ($('.header').innerHeight() + 50),
            scope = height_main / height_main__curr;

        $main.css({
            'height': height_main + 'px',
        });
        $iframe.css({
            'margin-top': (-((height_iframe_curr - (height_iframe_curr * scope)) / 2)) + 'px',
            'transform': 'rotate(-1deg) scale(' + scope + ')',
            '-o-transform': 'rotate(-1deg) scale(' + scope + ')',
            '-ms-transform': 'rotate(-1deg) scale(' + scope + ')',
            '-moz-transform': 'rotate(-1deg) scale(' + scope + ')',
            '-webkit-transform': 'rotate(-1deg) scale(' + scope + ')',
        });
    }
    else {
        $main.removeAttr('style');
        $iframe.removeAttr('style');
    }
}