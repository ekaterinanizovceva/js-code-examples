jQuery(function($) {

	var pharmacys = {
		container: $('.tm-pharmacys'),

		init: function() {
			pharmacys.getPharmacies(0, 0, 0);
			pharmacys.searchPharmacies();
		},
		getPharmacies: function(city, name, address) {
			$.getJSON('pharmacys.json', function(data){
				pharmacys.displayPharmacies(data.pharmacys, city, name, address);

				if (city == 0 && name == 0 && address == 0) {
					pharmacys.displaySearchForm(data.pharmacys);
					pharmacys.displayStatus(data.pharmacys);
				}
			});
		},
		displayPharmacies: function(listPharmacys, city, name, address) {
			$.each(listPharmacys, function(pharmacy_key, pharmacy) {
				if ((pharmacy.city == city || city == 0) && (pharmacy.name == name || name == 0) && (pharmacy.address == address || address == 0)) {
					var pharmacy_tr = $('<tr/>', {
						'class': 'tm-pharmacys__item'
					}).appendTo(pharmacys.container.find('.tm-pharmacys-list tbody'));

					$.each(pharmacy, function(key, val) {

						switch (key) {
							case 'name':
								$('<td/>', {
									'class': 'tm-pharmacys__item-name',
									'data-head': 'название аптеки'
								}).html('<a href="farmacevt.html?id=' + pharmacy.id + '">' + val + '</a><p>' + pharmacy.city + ', ' + pharmacy.address + '</p>').appendTo(pharmacy_tr);
								break;
							case 'status':
								$('<td/>', {
									'data-head': 'статус'
								}).text(val).appendTo(pharmacy_tr);
								break;
							case 'pharmacists':
								$('<td/>', {
									'data-head': 'фармацевты'
								}).text(val.length).appendTo(pharmacy_tr);
								break;
							case 'activity':
								$('<td/>', {
									'class': 'tm-pharmacys__item-activity',
									'data-head': 'активность'
								}).text(val).appendTo(pharmacy_tr);
								break;
						}
					});
				}
			});
		},
		displaySearchForm: function(listPharmacys) {
			var searchForm = $('.tm-pharmacys').find('.tm-pharmacys__search'),
				city = [],
				name = [],
				address = [];
			$.each(listPharmacys, function(pharmacy_key, pharmacy) {

				if ($.inArray(pharmacy.city, city) < 0) {
					city.push(pharmacy.city);
					searchForm.find('.tm-pharmacys__search-city').append('<option value="' + pharmacy.city + '">' + pharmacy.city + '</option>');
				}
				if ($.inArray(pharmacy.name, name) < 0) {
					name.push(pharmacy.name);
					searchForm.find('.tm-pharmacys__search-name').append('<option value="' + pharmacy.name + '">' + pharmacy.name + '</option>');
				}
				if ($.inArray(pharmacy.address, address) < 0) {
					address.push(pharmacy.address);
					searchForm.find('.tm-pharmacys__search-address').append('<option value="' + pharmacy.address + '">' + pharmacy.address + '</option>');
				}
			});
		},
		displayStatus: function(listPharmacys) {
			var searchStatus = $('.tm-pharmacys').find('.tm-pharmacys__status'),
				vipStatus = 0,
				standardStatus = 0,
				economyStatus = 0;

			$.each(listPharmacys, function(pharmacy_key, pharmacy) {
				switch(pharmacy.status) {
					case 'vip':
						vipStatus ++;
						break;
					case 'cтандарт':
						standardStatus ++;
						break;
					case 'эконом':
						economyStatus ++;
						break;
				}
			});

			searchStatus.find('[data-status="all"]').find('sup').text(listPharmacys.length);
			searchStatus.find('[data-status="vip"]').find('sup').text(vipStatus);
			searchStatus.find('[data-status="standard"]').find('sup').text(standardStatus);
			searchStatus.find('[data-status="economy"]').find('sup').text(economyStatus);
		},
		searchPharmacies: function() {
			pharmacys.container.find('.tm-pharmacys__search select').change(function() {
				pharmacys.container.find('.tm-pharmacys-list tbody').empty();

				var city = pharmacys.container.find('.tm-pharmacys__search .tm-pharmacys__search-city').val();
				var name = pharmacys.container.find('.tm-pharmacys__search .tm-pharmacys__search-name').val();
				var address = pharmacys.container.find('.tm-pharmacys__search .tm-pharmacys__search-address').val();
			    
			    pharmacys.getPharmacies(city, name, address);
			});
		}
	};

	pharmacys.init();
	
});
